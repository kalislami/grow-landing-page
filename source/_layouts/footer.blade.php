<div class="row justify-content-center" style="background-color: #f7f9fa;">
    <div class="col-md-8 col-lg-6 py-5 text-center">
        <h3 class="text-weight-light">Want to talk more about this? Get in touch.</h3>

        <div class="row mt-5">
            <div class="col-md-8">
                <form role="form" method="post"
                      action="https://getform.io/f/1f5d9b33-58f9-4edb-94db-074986d71623" class="text-left">

                    <div class="form-group">
                        <input id="name" type="text" name="name" class="form-control form-control-sm"
                               placeholder="Your full name" required>
                    </div>

                    <div class="form-group">
                        <input id="email" type="email" name="email" class="form-control form-control-sm"
                               placeholder="Your E-Mail" required>
                    </div>

                    <div class="form-group">
                                    <textarea class="form-control form-control-sm" name="messages" id="messages"
                                              cols="5" rows="3" placeholder="Your messages"
                                              required></textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-sm btn-block btn-success">Send us a message
                        </button>
                    </div>

                </form>
            </div>

            <div class="col-md-4 text-left">
                <h5 class="font-weight-bold">{{ $page->appName }}</h5>
                <h6>PT Global Konten Adikarya</h6>
                <small class="font-weight-light">
                    APL Office Tower, Lantai 16 Unit 9,
                    Podomoro City (Central Park),
                    Jl. Let. Jend. S. Parman, Kav. 28
                    Jakarta 11470 - Indonesia
                </small>
            </div>
        </div>
    </div>
</div>