<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title> {{ $page->appName }} | Scale your agency </title>
    <meta name="description" content="{{ $page->appDesc }}"/>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="icon" type="image/png" href="{{ $assets_path }}assets/images/favicon.png"/>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@CollisionC2">
    <meta name="twitter:title" content="{{ $page->appName }} | {{ $page->appShortDesc }}">
    <meta name="twitter:description" content="{{ $page->appDesc }}">
    <meta name="twitter:image" content="{{ $page->baseUrl . 'assets/images/meta-logo.png' }}">

    <!-- Open Graph data -->
    <meta property="og:title" content="{{ $page->appName }} | {{ $page->appShortDesc }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ $page->baseUrl }}"/>
    <meta property="og:image" content="{{ $page->baseUrl . 'assets/images/meta-logo.png' }}"/>
    <meta property="og:description" content="{{ $page->appDesc }}"/>

    <!-- Style & Script -->
    <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
    <script type="text/javascript" src="{{ mix('js/main.js', 'assets/build') }}"></script>

    @if($page->production)
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137201219-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', 'UA-137201219-1');
        </script>
    @endif

</head>