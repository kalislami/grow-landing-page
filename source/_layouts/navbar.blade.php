<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-white shadow">

    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="{{ $assets_path }}assets/images/content-grow-navbar-logo.png" width="auto" height="50" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>

            <ul class="navbar-nav ml-auto">
                {{--<li class="nav-item">--}}
                    {{--<a href="/" class="nav-link">Home</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a href="{{ $assets_path }}how-it-works/" class="nav-link">How it works</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a href="{{ $assets_path }}pricing/" class="nav-link">Pricing</a>--}}
                {{--</li>--}}
                <li class="nav-item">
                    <a href="{{ $assets_path }}freelancer/" class="nav-link">Freelancers</a>
                </li>
                <li class="nav-item">
                    <a href="{{ $assets_path }}faq/" class="nav-link">FAQ</a>
                </li>
                <li class="nav-item">
                    <a href="{{ $page->signInUrl }}" target="_blank" class="nav-link">Sign In</a>
                </li>
                <li class="nav-item">
                    <a href="{{ $page->signUpURL }}" class="nav-link" target="_blank">Signup</a>
                </li>
            </ul>
        </div>
    </div>

</nav>