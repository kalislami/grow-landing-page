@extends('_layouts.master', ['assets_path' => '../'])

@section('content')
    <div class="container-fluid">

        <div class="row justify-content-center py-5 mt-5">
            <h1 class="my-5">Here’s how {{ $page->appName }} works:</h1>
            <div class="col-lg-8 col-lg-offset-1">
                <iframe class="card shadow" width="100%" height="400" src="https://www.youtube.com/embed/CaUWG1rOfw4" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
            </div>
        </div>

        <div class="row justify-content-center py-5">
            <div class="col-md-8 col-11 p-0">
                <div class="card shadow border-0" style="border-radius: 10px">
                    <div class="card-body text-center p-5">
                        <h3 class="mb-5 font-weight-lighter">A simple and effective software for scaling your creative agency</h3>
                        <a href="{{ $page->signUpURL }}" class="btn btn-success _2-button mb-3 mb-lg-0">SIGNUP</a>
                        <a href="{{ $page->requestDemo }}" class="btn btn-success _2-button clear">Request demo</a>
                    </div>
                </div>
            </div>
        </div>

        @include('_layouts.footer')

    </div>
@endsection