@extends('_layouts.master', ['assets_path' => '../'])

@section('content')

    <div class="container-fluid">

        <div class="row justify-content-center py-5 my-5">
            <div class="col-md-10 offset-md-0">
                <h1 class="my-5 text-center">General FAQ</h1>
                <div id="accordion">

                    <div class="card">
                        <div class="card-header bg-white text-success border-bottom-0" id="headingOne">
                            <h5 class="mb-0">
                                <a class="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    How is ContentGrow different from other freelance e-marketplaces?
                                </a>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                The short answer is that ContentGrow is not designed to be an e-marketplace.
                                It is meant to be a collaboration platform for companies to work with their
                                existing teams of content creators, editors, and project managers.
                                <br><br>
                                Companies may also invite new talent, sourced from the outside world, into their projects on ContentGrow.
                                In the future, we may let companies browse shortlisted talent on the platform. Please stay tuned.
                                <br><br>
                                That said, if you need talent straight away, we can help curate some early users as a courtesy to get you started.
                                <a href="https://docs.google.com/forms/d/1lGx2XK7dDpRApfJiQLsXTlCIro7VXuyU_-FP-lTpEig/edit?ts=5c8b4509"
                                   target="_blank" class="link">
                                    Click here
                                </a>
                                to request your first contributors! Our team will get in touch.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bg-white text-success border-bottom-0">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapseOne">
                                    Can I browse and access the talent in ContentGrow's user base?
                                </a>
                            </h5>
                        </div>

                        <div id="collapse2" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Currently, we offer freelancer curation from our user base as a value-added service. In the future, we may let company users browse shortlisted talent on the platform. Please stay tuned for updates.
                                <br><br>
                                <a href="https://docs.google.com/forms/d/1lGx2XK7dDpRApfJiQLsXTlCIro7VXuyU_-FP-lTpEig/edit?ts=5c8b4509"
                                   target="_blank" class="link">
                                    Click here
                                </a>
                                to have our team curate talent for your first project!
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bg-white text-success border-bottom-0">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapseOne">
                                    How much should I pay freelancers from ContentGrow?
                                </a>
                            </h5>
                        </div>

                        <div id="collapse3" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                This will depend on your budget and how much you are willing to pay per deliverable.
                                Because every project and assignment is different, these fees will surely vary.
                                As the company, it’s up to you to dictate the rates based on your own circumstances.
                                Freelancers should be given the choice to opt-in or opt-out of your project ahead of time.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bg-white text-success border-bottom-0">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapseOne">
                                    Can I make payments to content creators via the platform?
                                </a>
                            </h5>
                        </div>

                        <div id="collapse4" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                This feature will likely be available in the future. For now, while ContentGrow is still new,
                                company users are only able to view the amounts owed to freelancers based on
                                assignments that have been completed and closed.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bg-white text-success border-bottom-0">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapseOne">
                                    How is ContentGrow different from other project management tools?
                                </a>
                            </h5>
                        </div>

                        <div id="collapse5" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                The short answer is focus. Unlike most other project management tools,
                                ContentGrow is one of the only platforms that is specifically designed for companies
                                that deal with high volumes of content creation (<strong>e.g.</strong> writing firms, marketing agencies,
                                media publishers, etc).
                                <br><br>
                                ContentGrow zeros in on key processes related to the flow of editorial work and necessary
                                quality control that goes with it. The platform has simplified or automated a variety of
                                key process that agencies usually handle manually (<strong>e.g.</strong> building briefs, assigning and
                                reminding contributors of tasks, harvesting creative pitches at scale,
                                imposing quality control guardrails, calculating fluid monthly payments, and more).
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bg-white text-success border-bottom-0">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapseOne">
                                    What currencies does ContentGrow support?
                                </a>
                            </h5>
                        </div>

                        <div id="collapse6" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Currently, fees on ContentGrow are supported using Indonesian Rupiah (IDR) and United States Dollars (USD).
                                This is because we are an Indonesia-based company that works with local and international talent.
                                Other currencies may be added in the future depending on demand.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bg-white text-success border-bottom-0">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapseOne">
                                    How can I be sure my projects remain confidential?
                                </a>
                            </h5>
                        </div>

                        <div id="collapse7" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Only you and the users you invite to a specific project can see its details.
                                No one on ContentGrow can see you or the projects you’re working on unless you make it happen.
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bg-white text-success border-bottom-0">
                            <h5 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapseOne">
                                    How much does ContentGrow cost?
                                </a>
                            </h5>
                        </div>

                        <div id="collapse8" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Currently, it’s free to start using ContentGrow! Just
                                <a href="http://platform.contentcollision.co/client-register" target="_blank" class="link">
                                    sign up here
                                </a>
                                as a company to get started.
                                In the future, we are likely to implement a ‘freemium’ business model.
                                For now, we’re merely seeking constructive input, feedback, and feature requests from
                                real users to help ContentGrow become awesome.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        @include('_layouts.footer')

    </div>
@endsection