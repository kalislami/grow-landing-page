@extends('_layouts.master', ['assets_path' => '../'])

@section('content')
    <div class="container-fluid">

        <div class="row justify-content-center py-5 mt-5">
            <div class="col-lg-8 pl-sm-5 pl-lg-0 pr-lg-0">
                <div class="row h-100 mb-2">
                    <div class="col-md-6">
                        <div class="mt-5 py-2">
                            <h1 class="mb-5">
                                Simple Pricing
                            </h1>
                            <p>
                                Easy to work with. No hidden fees.<br>
                                Annual and monthly billing options.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center py-5">
            <div class="col-md-10 col-md-offset-1">
                <div class="table-responsive">
                    <table border="0" cellpadding="10" cellspacing="1">
                        <thead>
                        <tr class="border-bottom">
                            <th width="25%">Our Plans</th>
                            <th>Lite</th>
                            <th>Starter</th>
                            <th>Pro</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                            <td><h4>$40 / mo</h4></td>
                            <td><h4>$60 / mo</h4></td>
                            <td><h4>$90 / mo</h4></td>
                        </tr>
                        <tr class="text-muted">
                            <td></td>
                            <td>per active user billed annually $50 when billed monthly</td>
                            <td>per active user billed annually $80 when billed monthly</td>
                            <td>per active user billed annually $120 when billed monthly</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td> <a class="btn btn-success _2-button clear"> Get Started</a> </td>
                            <td> <a class="btn btn-success _2-button clear"> Get Started</a> </td>
                            <td> <a class="btn btn-success _2-button clear"> Get Started</a> </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Lots of power at a low price for individu</td>
                            <td>Advanced features in an accessible package for small sales teams</td>
                            <td>Professional grade for proactive sales teams</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include('_layouts.footer')

    </div>
@endsection