@extends('_layouts.master', ['assets_path' => '../'])

@section('content')
    @php
        $json = file_get_contents('./freelancer.json');
        $freelancers = json_decode($json,true);
    @endphp
    <div class="container-fluid">

        <div class="row justify-content-center py-5 mt-5">
            <div class="col-md-10 offset-md-0">
                <h1 class="my-5 text-center">{{ $page->appName }} has talented people ready to work on your project!</h1>
                <div class="row">
                    @foreach($freelancers as $freelancer)
                        <div class="col-lg-4 col-md-6">
                            <div class="card cg-card mb-4 border-0 bg-light shadow">
                                <div class="card-header border-bottom-0">
                                    <span class="badge badge-success float-right mt-2">{{ $freelancer['roles'] }}</span>
                                    <h5>{{ $freelancer['name'] }}</h5>
                                </div>
                                <div class="card-body text-secondary">
                                    <p class="mb-0"><strong>Languages:</strong></p>
                                    <p class="ml-2"> {{ $freelancer['languages'] }}</p>
                                    <p class="mb-0"><strong>Specialities:</strong></p>
                                    <p class="ml-2">{{ $freelancer['specialities'] }}</p>
                                    <p class="text-center mb-0">
                                        <a href="https://docs.google.com/forms/d/1lGx2XK7dDpRApfJiQLsXTlCIro7VXuyU_-FP-lTpEig/edit?ts=5c8b4509"
                                           target="_blank" class="btn btn-sm btn-success">Get talent</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>


        @include('_layouts.footer')

    </div>
@endsection