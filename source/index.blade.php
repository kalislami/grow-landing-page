@extends('_layouts.master', ['assets_path' => ''])

@section('content')
    <div class="container-fluid">

        <div class="row justify-content-center" id="section1">

            <div class="col-lg-8 pl-sm-5 pl-lg-0 pr-lg-0">
                <div class="row h-100 mb-2">
                    <div class="col-md-6">
                        <div class="mt-5 py-5">

                            <h1 class="mb-5">
                                Think it’s impossible to scale the creative agency business model? Think again.
                            </h1>

                            <p>
                                We get it. Working with freelancers is hard.
                                Don’t make it harder on yourself.
                                {{ $page->appName }} helps your agency scale up by keeping out-of-house creative teams on
                                brief, ahead of deadline, in-the-know, and at your fingertips.
                            </p>

                            <p>
                                Stop stacking fixed overhead costs and go lean. There’s a better way to do this.
                            </p>

                            <p>
                                {{ $page->appName }}'s tech has empowered agencies to serve blue-chip clients like:
                            </p>

                            <div class="row mt-sm-5 mt-3">

                                <div class="col-md-4 p-5 p-sm-2 col-6">
                                    <img src="assets/images/user/scmp.png" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-4 p-5 p-sm-2 col-6">
                                    <img src="assets/images/user/lazada.png" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-4 p-5 p-sm-2 col-6">
                                    <img src="assets/images/user/reuters.png" class="img-fluid" alt="">
                                </div>

                                <div class="col-md-4 p-5 p-sm-2 col-6">
                                    <img src="assets/images/user/zomato.png" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-4 p-5 p-sm-2 col-6">
                                    <img src="assets/images/user/cnn.png" class="img-fluid" alt="">
                                </div>
                                <div class="col-md-4 p-5 p-sm-2 col-6">
                                    <img src="assets/images/user/blibli.png" class="img-fluid" alt="">
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="row justify-content-center py-5">
            <div class="col-md-8 col-11 p-0">
                <div class="card shadow border-0" style="border-radius: 10px">
                    <div class="card-body text-center p-5">
                        <h3 class="mb-5 font-weight-lighter">A simple and effective software for scaling your creative agency</h3>
                        <a href="{{ $page->signUpURL }}" class="btn btn-success _2-button mb-3 mb-lg-0">SIGNUP</a>
                        <a href="{{ $page->requestDemo }}" class="btn btn-success _2-button clear">Request demo</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" style="background-image:linear-gradient(white, #f7f9fa);">

            <div class="col-md-8 my-5 text-center">
                <h3>What makes {{ $page->appName }} <i>better</i>?</h3>
            </div>

            <div class="col-md-8">

                <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                    <li class="nav-item c2-preview">
                        <a class="nav-link active" id="client-tab" data-toggle="tab" href="#client" role="tab"
                           aria-controls="client" aria-selected="true">Company Dashboard</a>
                    </li>
                    <li class="nav-item c2-preview">
                        <a class="nav-link text-success" id="contributor-tab" data-toggle="tab" href="#contributor"
                           role="tab" aria-controls="contributor" aria-selected="false">Contributor Dashboard</a>
                    </li>
                </ul>

                <div class="tab-content" id="myTabContent">
                    <div class="py-4 tab-pane fade show active" id="client" role="tabpanel" aria-labelledby="client-tab">

                        <div class="row">
                            <div class="col-lg-3">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                     aria-orientation="vertical">

                                    <a class="nav-link active" id="v-pills-dashboard-detail-tab" data-toggle="pill"
                                       href="#v-pills-dashboard-detail" role="tab" aria-controls="v-pills-dashboard-detail"
                                       aria-selected="true">Dashboard</a>

                                    <a class="nav-link" id="v-pills-project-detail-tab" data-toggle="pill"
                                       href="#v-pills-project-detail" role="tab" aria-controls="v-pills-project-detail"
                                       aria-selected="true">Project</a>

                                    <a class="nav-link" id="v-pills-project-team-tab" data-toggle="pill"
                                       href="#v-pills-project-team" role="tab" aria-controls="v-pills-project-team"
                                       aria-selected="false">Contributors</a>
                                </div>
                            </div>

                            <div class="col-lg-9">
                                <div class="tab-content" id="v-pills-tabContent">

                                    <div class="tab-pane fade show active" id="v-pills-dashboard-detail" role="tabpanel"
                                         aria-labelledby="v-pills-project-detail-tab">
                                        <img src="assets/images/client/Dashboard.png" class="d-block w-100" alt="...">
                                    </div>

                                    <div class="tab-pane fade" id="v-pills-project-detail" role="tabpanel"
                                         aria-labelledby="v-pills-project-detail-tab">
                                        <div class="bd-example">
                                            <div id="carouselProject" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <img src="assets/images/client/project/Project%20Details.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/client/project/Project%20Team.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/client/project/Project%20Pitch.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/client/project/Project%20Articles.png" class="d-block w-100" alt="...">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="v-pills-project-team" role="tabpanel"
                                         aria-labelledby="v-pills-project-team-tab">

                                        <div class="bd-example">
                                            <div id="carouselClientContributor" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <img src="assets/images/client/contributor/contributor-list.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/client/contributor/invite-new-contributor.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/client/contributor/invite-existing-contributor.png" class="d-block w-100" alt="...">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="py-4 tab-pane fade" id="contributor" role="tabpanel" >
                        <div class="row">

                            <div class="col-lg-3">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                     aria-orientation="vertical">

                                    <a class="nav-link active" id="v-pills-contributor-dashboard-tab" data-toggle="pill"
                                       href="#v-pills-contributor-dashboard" role="tab" aria-controls="v-pills-contributor-dashboard"
                                       aria-selected="true">Dashboard</a>

                                    <a class="nav-link" id="v-pills-contributor-project-tab" data-toggle="pill"
                                       href="#v-pills-contributor-project" role="tab" aria-controls="v-pills-contributor-project"
                                       aria-selected="false">Project</a>

                                    <a class="nav-link" id="v-pills-contributor-assignment-tab" data-toggle="pill"
                                       href="#v-pills-contributor-assignment" role="tab" aria-controls="v-pills-contributor-assignment"
                                       aria-selected="false">Assignment</a>

                                </div>
                            </div>

                            <div class="col-lg-9">
                                <div class="tab-content" id="v-pills-tabContent">

                                    <div class="tab-pane fade show active" id="v-pills-contributor-dashboard" role="tabpanel"
                                         aria-labelledby="v-pills-contributor-project-tab">

                                        <div class="bd-example">
                                            <div id="carouselContributorDashboard" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <img src="assets/images/contributor/dashboard/my-assignment.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/contributor/dashboard/my-assignment.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/contributor/dashboard/my-assignment.png" class="d-block w-100" alt="...">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="tab-pane fade" id="v-pills-contributor-project" role="tabpanel"
                                         aria-labelledby="v-pills-contributor-project-tab">

                                        <div class="bd-example">
                                            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <img src="assets/images/contributor/project/project-detail.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/contributor/project/project-my-pitch.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/contributor/project/project-team-pitches.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/contributor/project/project-articles.png" class="d-block w-100" alt="...">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="tab-pane fade" id="v-pills-contributor-assignment" role="tabpanel"
                                         aria-labelledby="v-pills-contributor-assignment-tab">

                                        <div class="bd-example">
                                            <div id="carouselContributorAssignment" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <div class="carousel-item active">
                                                        <img src="assets/images/contributor/assignment/active-assignment.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/contributor/assignment/claim-assignment.png" class="d-block w-100" alt="...">
                                                    </div>
                                                    <div class="carousel-item">
                                                        <img src="assets/images/contributor/assignment/finished-assignment.png" class="d-block w-100" alt="...">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center py-5" style="background-image:linear-gradient(#f7f9fa, white);">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-6">

                        <h4 class="mb-4">
                            Some agencies think sourcing good and affordable talent is their main problem. <br> <b>It’s
                                not</b>.
                        </h4>

                        <p class="font-weight-light">
                            The reason most creative agencies have trouble scaling up with freelancers is not that the
                            people they find lack talent.
                            On the contrary.
                            It’s because most agencies lack the digital framework needed to verify that instructions are
                            fully understood and quality control layers are strictly imposed.
                        </p>

                        <p class="font-weight-light">
                            {{ $page->appName }} solves this by making briefs fool-proof and giving every project a proper hierarchy of
                            team members.
                        </p>

                    </div>

                    <div class="col-lg-6 pt-5 pt-lg-0 px-5">
                        <div class="card shadow border-0">
                            <div class="card-body">
                                <h6 class="text-info">Rigorous quality control is the name of the game</h6>
                                <p class="font-weight-light">
                                    No assignment from your agency will ever be delivered to your client without first being double checked by a layer of editors and project managers.
                                    Our system of checks and balances keep your deliverables sharp and on-point.
                                </p>

                                <hr>

                                <h6 class="text-info">Automated reminders make deadlines a top priority</h6>
                                <p class="font-weight-light">
                                    If you’ve ever tried to manage a large, decentralized team of content producers while adhering to a strict schedule, then you know how challenging it is to keep tabs on everyone.
                                    {{ $page->appName }} does this for you!
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" id="cta1">
            <div class="col-md-8 p-5 text-center">
                <h1 class="mb-5">{{ $page->appName }} is self-serve. Give it a try!</h1>

                <h5>
                    The best part? Try {{ $page->appName }} on your own. Free for two weeks. No demos or calls with our sales team until you're ready — in which case we’d love to chat.
                </h5>

                <div class="my-5">
                    <a href="{{ $page->signUpURL }}" class="btn btn-success _2-button mb-3 mb-lg-0">SIGNUP</a>
                    <a href="{{ $page->requestDemo }}" class="btn btn-success _2-button clear">Request demo</a>
                </div>

            </div>
        </div>

        <div class="row justify-content-center" id="fact2">
            <div class="col-md-8 py-5">
                <div class="row">
                    <div class="col-md-6 py-4">

                        <h4 class="mb-4">
                            Some agencies think it’s too hard to keep out-of-house <b>teams organized and on time</b>.
                        </h4>

                        <p class="font-weight-light">
                            It’s true. Working with a large group of freelancers can feel like herding a bunch of unruly
                            cats.
                            This is not the case with {{ $page->appName }}.
                        </p>

                        <p class="font-weight-light">
                            Our platform solves this by ensuring teams keep up with deadlines via automated reminders
                            and assignment progress tracking.
                            Each team member has a task to do on an assignment. They pass the ball back and forth until
                            completion.
                        </p>

                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center py-5" style="background-color: #f7f9ff">
            <div class="col-md-8 p-5 text-center">
                <h1 class="mb-5">{{ $page->appName }} was built to make both sides of the creative business equation happy!</h1>

                <div class="row">

                    <div class="col-md-6 text-left">
                        <h5 class="text-info mb-4">Agencies</h5>

                        <p class="font-weight-light">
                            Get the best of both worlds! Stop hiring in-house writers, editors, and designers based on demands from clients that ultimately come and go.
                        </p>

                        <p class="font-weight-light">
                            Let your core in-house team take advantage of a system that yields high-quality content at scale.

                            Keep content creators, editors, and managers synced and sequenced. Lighten your own mental load and effectively delegate creative work in our playground.
                        </p>
                    </div>

                    <div class="col-md-6 text-left">
                        <h5 class="text-info mb-4">Contributors</h5>

                        <p class="font-weight-light">
                            No longer do you need to cope with unclear client briefs or continuously be reminded about deadlines.
                            With {{ $page->appName }}, all open assignments appear on a dashboard and you'll always know who’s turn it is to take action!
                        </p>

                        <p class="font-weight-light">
                            Crystal clear briefs coupled with pre-determined editors and project managers, you’re now free to remove bureaucracy from your craft and focus on what really matters: <b>producing great content!</b>
                        </p>
                    </div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center py-5" id="cta3">
            <div class="col-md-6 py-5 text-center">
                <h1 class="mb-5 text-white">It's quick and easy! Let {{ $page->appName }} help scale your creative agency.</h1>
                <div class="my-5">
                    <a href="{{ $page->signUpURL }}" class="btn btn-success _2-button mb-3 mb-lg-0">SIGNUP</a>
                    <a href="{{ $page->requestDemo }}" target="_blank" class="btn btn-success _2-button clear">Request demo</a>
                </div>
            </div>
        </div>

        @include('_layouts.footer')

    </div>
@endsection