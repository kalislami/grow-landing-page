<?php

return [
    'appName' => 'ContentGrow',
    'appDesc' => 'Scaling your agency with freelancers is not just a dream anymore. With ContentGrow you can organize freelancers and manage the work without breaking a sweat!',
    'appShortDesc' => 'Scale your agency',
    'baseUrl' => 'https://www.contentgrow.com/',
    'collections' => [],
    'production' => true,
    'staging' => false,
    'signInUrl' => 'http://platform.contentcollision.co',
    'signUpURL' => 'http://platform.contentcollision.co/client-register',
    'requestDemo' => "mailto:leighton@contentcollision.co?subject=Request For Demo Of Content Grow&body=Hi, I'd like to try your software"
];
