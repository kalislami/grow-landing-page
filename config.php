<?php

return [
    'appName' => 'ContentGrow',
    'appDesc' => 'Scaling your agency with freelancers is not just a dream anymore. With ContentGrow you can organize freelancers and manage the work without breaking a sweat!',
    'appShortDesc' => 'Scale your agency',
    'baseUrl' => 'http://mock.c2work.com/',
    'collections' => [],
    'production' => false,
    'staging' => true,
    'signInUrl' => 'https://stgplat.c2work.com',
    'signUpURL' => 'https://stgplat.c2work.com/client-register',
    'requestDemo' => "mailto:leighton@contentcollision.co?subject=Request For Demo Of Content Grow&body=Hi, I'd like to try your software"
];
